<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\SendMailController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\UserInvitationController;
use App\Http\Controllers\Admin\InvitationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/events/{user:uuid}', [UserInvitationController::class, 'invite'])->name('invites.create');
Route::post('/events/store', [UserInvitationController::class, 'process_invite'])->name('invites.process');
Route::get('/events/finish/{user:uuid}', [UserInvitationController::class, 'finish_invite'])->name('invites.finish');


Route::middleware('guest:web')->group(function() {
    Route::get('/login',[LoginController::class, 'login'])->name('login');
    Route::post('/login', [LoginController::class, 'processLogin'])->name('login.process');
});

Route::middleware('auth:web')->group(function() {
    Route::post('logout', [LoginController::class, 'logout'])->name('logout');

    Route::get('/', [UserController::class, 'index'])->name('users');
    Route::get('users/getdata', [UserController::class, 'get_data'])->name('users.getdata');
    Route::get('users/create', [UserController::class, 'create'])->name('users.create');
    Route::post('users/store', [UserController::class, 'store'])->name('users.store');
    Route::get('users/edit/{id}', [UserController::class, 'edit'])->name('users.edit');
    Route::patch('users/update/{id}', [UserController::class, 'update'])->name('users.update');
    Route::delete('users/delete/{id}', [UserController::class, 'destroy'])->name('users.destroy');
    Route::post('users/send', SendMailController::class)->name('users.send');

    Route::get('invitations', [InvitationController::class, 'index'])->name('invitations');
    Route::get('invitations/getdata', [InvitationController::class, 'get_data'])->name('invitations.getdata');
});
