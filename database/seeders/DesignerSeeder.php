<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DesignerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $designers = [
            [
                'name' => 'Alexander McQueen'
            ],
            [
                'name' => 'Baby Dior'
            ],
            [
                'name' => 'Christian Lacroix'
            ],
            [
                'name' => 'Dolce Vita'
            ],
            [
                'name' => 'Elizabeth Arden'
            ]
        ];

        foreach ($designers as $designer) {
            \App\Models\Designer::create(['name' => $designer['name']]);
        }
    }
}
