# Getting started

## Installation

Clone the repository

    git clone git@bitbucket.org:PutraAdit/huntbazaar.git

Switch to the repo folder

    cd huntbazaar

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

**TL;DR command list**

    git clone git@bitbucket.org:PutraAdit/huntbazaar.git
    cd huntbazaar
    composer install
    cp .env.example .env
    php artisan key:generate
    
**Make sure you set the correct database connection information before running the migrations** [Environment variables](#environment-variables)

    php artisan migrate
    php artisan serve

## Database seeding

Run the database seeder

    php artisan db:seed --class=UserSeeder
    php artisan db:seed --class=DesignerSeeder

***Note*** : It's recommended to have a clean database before seeding. You can refresh your migrations at any point to clean the database by running the following command

    php artisan migrate:refresh
    
## Queue

Run the queue

    php artisan queue:work --daemon

***Note*** : For production, you should also use something like Supervisord to ensure that the service remains running and is restarted after crashes/failures

## Cron Job

Run the cron job

    php artisan schedule:run

***Note*** : For production, you should add the complete path in crontab file of your application, example

Add (Change `ypur-app-path`)

    * * * * * cd /your-app-path && php artisan schedule:run >> /dev/null 2>&1
