@extends('layouts.admin')
    @section('content')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Data User</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item active">Data User</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Data User</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <a href="{{ route('users.create') }}" class="btn btn-primary btn-sm move">+ Tambah User</a>
                                    <table class="table table-bordered data-table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>Tanggal Diperbarui</th>
                                                <th>Undangan</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    @endsection
    @section('js')
        <script>
            $(function () {
                var al = {!! json_encode(session('success')) !!};
                if (al) {
                    swal({
                        title: "Sukses",
                        text: al,
                        type: "success",
                    });
                }
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.fn.dataTable.ext.errMode = 'none';
                var table = $('.data-table').DataTable({
                    dom : 'l<"#DataTables_Table_0_length">frtip',
                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: "{{ route('users.getdata') }}",
                        type: "GET",
                        data: function (d) {
                            d.category = $('#cat_id').val();
                            d.status = $('#status').val();
                        }
                    },
                    order: [[3, 'desc']],
                    columns: [
                        {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                        {data: 'name', name: 'name'},
                        {data: 'email', name: 'email'},
                        {data: 'updated_at', name: 'updated_at'},
                        {data: 'send', name: 'send'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ],
                    "language": {
                        "lengthMenu": 'Show _MENU_',
                    }
                });
                $('#filterSearch').click(function(){
                    table.draw();
                });
                $('.dataTables_filter input[type="search"]').remove();
                $('#DataTables_Table_0_filter label').css({'display':'none'});
                $('.move').appendTo('#DataTables_Table_0_length').css({"margin-left":"10px"});
                $('body').on('click', '.deleteUser', function () {
                    var user_id = $(this).data("id");
                    swal({
                        title: "Hapus?",
                        text: "Apa anda yakin?",
                        type: "warning",
                        showCancelButton: !0,
                        confirmButtonText: "Ya",
                        cancelButtonText: "Tidak",
                        reverseButtons: !0
                    }).then(function (e) {

                        if (e.value === true) {
                            $.ajax({
                                type: "DELETE",
                                url: "/users/delete/" + user_id,
                                success: function (data) {
                                    if (data['success'] == false) {
                                        swal({
                                            title: "Gagal",
                                            text: "Tidak dapat menghapus data user",
                                            type: "error"
                                        });
                                    } else {
                                        table.draw();
                                    }
                                },
                                error: function (data) {
                                    console.log('Error:', data);
                                }
                            });
                        } else {
                            e.dismiss;
                        }

                    }, function (dismiss) {
                        return false;
                    })
                });

                $('body').on('click', '.sendEmail', function () {
                    var user_id = $(this).data("id");
                    $.ajax({
                        type: "POST",
                        url: "/users/send/",
                        data: {id:user_id},
                        success: function (data) {
                            if (data['success'] == false) {
                                swal({
                                    title: "Gagal",
                                    text: "Tidak dapat menghapus data user",
                                    type: "error"
                                });
                            } else {
                                console.log(data['data']);
                                swal({
                                    title: "Sukses",
                                    text: "Email Terkirim",
                                    type: "success"
                                });
                            }
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                });
            });
        </script>
    @endsection