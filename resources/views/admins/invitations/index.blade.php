@extends('layouts.admin')
    @section('content')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Data Undangan</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item active">Data Undangan</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Data Undangan</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <table class="table table-bordered data-table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nama</th>
                                                <th>Email</th>
                                                <th>Tanggal Lahir</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    @endsection
    @section('js')
        <script>
            $(function () {
                var al = {!! json_encode(session('success')) !!};
                if (al) {
                    swal({
                        title: "Sukses",
                        text: al,
                        type: "success",
                    });
                }
                $.fn.dataTable.ext.errMode = 'none';
                var table = $('.data-table').DataTable({
                    dom : 'l<"#DataTables_Table_0_length">frtip',
                    processing: false,
                    serverSide: true,
                    ajax: {
                        url: "{{ route('invitations.getdata') }}",
                        type: "GET",
                    },
                    order: [[1, 'desc']],
                    columns: [
                        {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                        {data: 'user.name', name: 'user.name'},
                        {data: 'user.email', name: 'user.email'},
                        {data: 'user.birthdate', name: 'user.birthdate'},
                        {data: 'user.gender', name: 'user.gender'},
                        {data: 'status', name: 'status'},
                    ],
                    "language": {
                        "lengthMenu": 'Show _MENU_',
                    }
                });
                $('.dataTables_filter input[type="search"]').remove();
                $('#DataTables_Table_0_filter label').css({'display':'none'});
            });
        </script>
    @endsection