@extends('layouts.user')
    @section('content')
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Invitation ID</p>

                <form method="post" action="#">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control text-center" value="{{ $invite->identity }}" readonly>
                    </div>
                </form>
            </div>
            <!-- /.login-card-body -->
        </div>
    @endsection