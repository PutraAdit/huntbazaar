@extends('layouts.user')
    @section('content')
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Isi Data Dibawah Ini</p>
                <p class="login-box-msg">*semua kolom wajib diisi</p>

                <form method="post" action="{{ route('invites.process') }}">
                    @csrf
                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                    <div class="input-group mb-3">
                        <input type="email" class="form-control" name="email" placeholder="Email" value="{{ $user->email }}" readonly>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Nama">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span></span>
                            </div>
                        </div>
                        <div class="invalid-feedback">
                            {{ $errors->first('name') }}
                        </div>
                    </div>
                    <div class="input-group date mb-3" id="date_in" data-target-input="nearest">
                        <input type="text" class="form-control @error('birthdate') is-invalid @enderror datetimepicker-input" name="birthdate" placeholder="Tanggal lahir" data-target="#date_in" value="{{ old('birthdate') }}"/>
                        <div class="input-group-append" data-target="#date_in" data-toggle="datetimepicker">
                            <div class="input-group-text">
                                <span class="fa fa-calendar"></span>
                            </div>
                        </div>
                        <div class="invalid-feedback">
                            {{ $errors->first('birthdate') }}
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <select class="form-control @error('gender') is-invalid @enderror" name="gender">
                            <option value="">Pilih jenis kelamin</option>
                            <option value="1">Laki - Laki</option>
                            <option value="0">Perempuan</option>
                        </select>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span></span>
                            </div>
                        </div>
                        <div class="invalid-feedback">
                            {{ $errors->first('gender') }}
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <select class="form-control @error('designer') is-invalid @enderror" name="designer[]" multiple>
                            @foreach($designers as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span></span>
                            </div>
                        </div>
                        <div class="invalid-feedback">
                            {{ $errors->first('designer') }}
                        </div>
                    </div>
                    <div class="row">
                        <!-- /.col -->
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block">Submit</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
            <!-- /.login-card-body -->
            <div class="card-body login-card-body">
                <p class="login-box-msg">Pendaftaran berakhir dalam waktu</p>
                <p class="login-box-msg" id="demo"></p>
            </div>
            <!-- /.login-card-body -->
        </div>
    @endsection
    @section('js')
        <script>
            // Set the date we're counting down to
            var countDownDate = new Date("Oct 3, 2021 00:00:00").getTime();

            // Update the count down every 1 second
            var x = setInterval(function() {

            // Get today's date and time
            var now = new Date().getTime();
        
            // Find the distance between now and the count down date
            var distance = countDownDate - now;
        
            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
            // Output the result in an element with id="demo"
            document.getElementById("demo").innerHTML = days + "d " + hours + "h "
                + minutes + "m " + seconds + "s ";
        
            // If the count down is over, write some text 
            if (distance < 0) {
                clearInterval(x);
                document.getElementById("demo").innerHTML = "EXPIRED";
            }
        }, 1000);
    </script>
    @endsection