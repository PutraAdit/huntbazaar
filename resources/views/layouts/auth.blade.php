<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>HUNTBAZAAR DASHBOARD</title>

		<!-- Google Font: Source Sans Pro -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="{{ asset('css/all.min.css') }}">
		<!-- icheck bootstrap -->
		<link rel="stylesheet" href="{{ asset('css/icheck-bootstrap.min.css') }}">
		<!-- Theme style -->
		<link rel="stylesheet" href="{{ asset('css/adminlte.min.css') }}">
		<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/clipart.png') }}">
	</head>
	<body class="hold-transition login-page">
		<div class="login-box">
			<div class="login-logo">
				<a href="{{ route('login') }}">
					<img src="{{ asset('images/clipart.png') }}" alt="Huntbazaar" class="brand-image" style="max-height: 100px !important;">
					<br>
					<b>HUNT</b>BAZAAR
				</a>
			</div>
			<!-- /.login-logo -->
			@yield('content')
		</div>
		<!-- /.login-box -->

		<!-- jQuery -->
		<script src="{{ asset('js/jquery.min.js') }}"></script>
		<!-- Bootstrap 4 -->
		<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
		<!-- AdminLTE App -->
		<script src="{{ asset('js/adminlte.min.js') }}"></script>
	</body>
</html>