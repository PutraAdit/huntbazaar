<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InviteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'name'  => 'required',
            'birthdate' => 'required',
            'gender' => 'required',
            'designer' => "required|array|min:1",
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Nama harus diisi',
            'birthdate.required'  => 'Tanggal lahir harus diisi',
            'gender.required' => 'Pilih salah satu jenis kelamin',
            'designer.min' => 'Pilih minimal 1 designer',
            'designer.required' => 'Pilih minimal 1 designer'
        ];
    }
}
