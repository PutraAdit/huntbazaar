<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\UserTrait;

class LoginController extends Controller
{
    use UserTrait;

    public function login()
    {
        return view('admins.auth.login');
    }

    public function processLogin(Request $request)
    {   
        $credentials = $request->except(['_token']);
        
        if(UserTrait::user_is_admin($request->email))
        {
            if(Auth::guard('web')->attempt($credentials))
            {   
                return redirect()->route('users');
            }
            return redirect()->action([
                LoginController::class,
                'login'
            ])->with('message','Credentials not matced in our records!');
        }
        return redirect()->action([
            LoginController::class,
            'login'
        ])->with('message','You are not an admin');
    }

    public function logout()
    {
        Auth::guard('web')->logout();
        return redirect()->action([
            LoginController::class,
            'login'
        ]);
    }
}
