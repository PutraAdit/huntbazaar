<?php

namespace App\Http\Controllers;

use App\Http\Requests\InviteRequest;
use App\Models\User;
use App\Models\Designer;
use App\Models\Invitation;
use App\Jobs\ThankEmailJob;
use App\Http\Traits\UserTrait;

class UserInvitationController extends Controller
{
    use UserTrait;

    public function invite($uuid)
    {
        if (UserTrait::expired_invitation()) {
            return view('invitations.expired');
        }
        $invite = Invitation::userUuid($uuid)->identity()->first();
        if($invite) {
            return redirect()->route('invites.finish', [$uuid]);
        }
        $user = User::whereUuid($uuid)->firstOrFail();
        $designers = Designer::pluck('name', 'id');
        return view('invitations.create', compact('user', 'designers'));
    }

    public function process_invite(InviteRequest $request)
    {
        try {
            $invite = (new Invitation())->invite_create($request->validated());
            $data = [
                'email' => $invite[1],
            ];
            $now = now();
            dispatch(new ThankEmailJob($data))->delay($now->addHour());
            return redirect()->route('invites.finish', [$invite[0]]);
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }

    public function finish_invite($uuid)
    {
        $invite = Invitation::userUuid($uuid)->firstOrFail();
        return view('invitations.finish', compact('invite'));
    }
}
