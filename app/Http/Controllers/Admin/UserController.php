<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use DataTables;

class UserController extends Controller
{
    public function index()
    {
        return view('admins.users.index');
    }

    public function get_data()
    {
        $data = User::userInvite()->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('updated_at', function($row) {
                return date('d-m-Y', strtotime($row->updated_at));
            })
            ->addColumn('send', function($row) {
                return '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Send" class="btn btn-default btn-sm sendEmail">Invite</a>';
            })
            ->addColumn('action', function($row) {
                $btn = '<a href="'. route('users.edit', $row->id) . '" class="edit btn btn-primary btn-sm">Edit</a>';
                $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteUser">Delete</a>';
                return $btn;
            })
            ->rawColumns(['action', 'send'])
            ->make(true);
    }

    public function create()
    {
        return view('admins.users.create');
    }

    public function store(UserRequest $request)
    {
        try {
            (new User())->create_or_update($request->validated(), 'create');
            return redirect()->route('users');
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('admins.users.edit', compact('user'));
    }
    
    public function update(UpdateUserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        try {
            $merge = array_merge($request->validated(), ['id' => $id]);
            $user->create_or_update($merge, 'update');
            return redirect()->route('users');
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }

    public function destroy($id)
    {
        try {
            User::find($id)->delete();
            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response()->json(['success' => false]);
        }
    }
}
