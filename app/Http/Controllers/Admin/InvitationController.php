<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Invitation;
use DataTables;

class InvitationController extends Controller
{
    public function index()
    {
        return view('admins.invitations.index');
    }

    public function get_data()
    {
        $data = Invitation::with('user')->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('user.birthdate', function($row) {
                return $row->user->birthdate? date('d-m-Y', strtotime($row->user->birthdate)) : '-';
            })
            ->editColumn('user.gender', function($row) {
                $gender = 'Perempuan';
                if (is_null($row->user->gender)) {
                    $gender = '-';
                } elseif ($row->user->gender) {
                    $gender = 'Laki - Laki';
                }
                return $gender;
            })
            ->editColumn('status', function($row) {
                switch ($row->status) {
                    case 'accept':
                        $status = 'User Confirm';
                        break;
                    case 'expired':
                        $status = 'Expired';
                        break;
                    default:
                        $status = 'Email Sent';
                        break;
                }
                return $status;
            })
            ->make(true);
    }
}
