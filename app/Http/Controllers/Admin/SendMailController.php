<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Jobs\SendEmailJob;

class SendMailController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        try {
            $user = User::findOrFail($request->input('id'));
            $data = [
                'email' => $user->email,
                'url' => url('/events' . '/' . $user->uuid)
            ];
            $now = now();
            dispatch(new SendEmailJob($data))->delay($now->addSeconds(10));
            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response()->json(['success' => false]);
        }
    }
}
