<?php

namespace App\Http\Traits;

use App\Models\User;

trait UserTrait
{
    public static function user_is_admin($email)
    {
        $user = User::whereEmail($email)->isAdmin()->exists();

        if($user)
        {
            return true;
        }
        return false;
    }

    public static function expired_invitation()
    {
        if (date('Y-m-d') >= date('Y-m-d', strtotime('2021-10-03'))) {
            return true;
        }
        return false;
    }
}