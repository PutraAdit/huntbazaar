<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Invitation;

class ExpiredCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expired:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change status invitation to expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $invites = Invitation::whereDate('created_at', '>', '2021-10-03')->get();
        foreach ($invites as $invite) {
            $invite->update(['status' => Invitation::EXPIRED]);
        }
    }
}
