<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use DB;

class Invitation extends Model
{
    use HasFactory;

    const SEND = 'send';
    const CONFIRMED = 'accept';
    const EXPIRED = 'expired';

    protected $fillable = [
        'user_id',
        'status',
        'identity'
    ];

    public function user()
    {
        return $this->belongsTo(User::class)->withDefault();
    }

    public function invite_create($data)
    {
        $invite = self::where('user_id', $data['user_id'])->first();
        $invite->user()->update([
            'name' => $data['name'],
            'birthdate' => date('Y-m-d H:i:s', strtotime($data['birthdate'])),
            'gender' => $data['gender'],
        ]);
        $invite->update([
            'identity' => (string) Str::random(10),
            'status' => self::CONFIRMED
        ]);
        foreach ($data['designer'] as $value) {
            DB::table('invitation_details')->insert([
                'invitation_id' => $invite->id,
                'designer_id' => $value,
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }

        return [$invite->user->uuid, $invite->user->email];
    }

    public function scopeUserUuid($query, $uuid)
    {
        return $query->with(['user' => function($q) use ($uuid) {
            $q->where('uuid', $uuid);
        }]);
    }

    public function scopeIdentity($query)
    {
        return $query->whereNotNull('identity');
    }
}
