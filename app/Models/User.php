<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use DB;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * Generate Uuid for Invitation
     *
     * @var string
     */
    protected static function  boot()
    {
        parent::boot();

        static::creating(function ($model)  {
            $model->uuid = (string) Str::uuid();
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'birthdate',
        'gender',
        'uuid',
        'is_admin'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeUserInvite($query)
    {
        return $query->where('is_admin', false);
    }

    public function scopeIsAdmin($query)
    {
        return $query->where('is_admin', true);
    }

    public function create_or_update($data, $param)
    {
        if ($param == 'create') {
            $user = self::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make('12345678'),
                'is_admin' => false
            ]);
            DB::table('invitations')->insert([
                'user_id' => $user->id,
                'status' => 'send',
                'created_at' => now(),
                'updated_at' => now()
            ]);
        } else {
            self::find($data['id'])->update([
                'name' => $data['name'],
                'email' => $data['email']
            ]);
        }
    }
}
